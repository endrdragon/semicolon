import datetime as dt
import os
import random
import re
import typing

import discord
import numpy as np
from discord.ext import commands

from cogs.utils.bot import Semicolon
from cogs.utils.errors import UserInputError
from cogs.utils.utils import JsonManager, Localizer, embed_author_template, unpack_args

top_cmd = 'top'


class GameScore:
    __slots__ = {"score", "success", "max", "user"}

    def __init__(self, score: typing.Union[int, float], success: bool = None, user: typing.Union[int, float] = None,
                 max_int: typing.Union[int, float] = None):
        self.score = score
        self.success = success
        self.max = max_int if max_int is not None else score  # what should be used for comparing maximums
        self.user = user


class Leaderboard(JsonManager):
    __slots__ = {"_localizer", "_max_str", "_board_str", "_game", "_global_key", "_top_keys", "_colours"}

    """
    Extend off this class with a _scoring(args) function and a game specified in super().__init__
    """
    def __init__(self, game: str, localizer: Localizer):
        self._localizer = localizer
        self._max_str = 'best'
        self._board_str = [self._max_str]
        self._game = game
        self._global_key = 'global_data'
        self._top_keys = {'user_data': 'ctx.author.id', self._global_key: "'global'"}  # use eval() on these
        self._colours = {0: discord.Colour.dark_blue(), 1: discord.Colour.dark_green(),
                         2: discord.Colour.green(), 3: discord.Colour.dark_purple()}
        super().__init__('leaderboard.json')

    def init_data(self, key: str, init_with=None):
        """Creates objects in json dict if non-existent"""
        super().init_data(key, init_with)
        if self._game not in self.data[key]:
            self.data[key][self._game] = {}
        for board_str in self._board_str:
            if board_str not in self.data[key][self._game]:
                self.data[key][self._game][board_str] = None

    def _save_score(self, snowflake: typing.Union[int, str], score: typing.Union[float, int]) -> GameScore:
        """Saves a score to json if it is a new best"""
        uid = str(snowflake)
        self.init_data(uid)
        old_max = self.data[uid][self._game][self._max_str]
        # grr this code repeats a little...
        new_record = True
        if old_max is not None and old_max >= score:
                new_record = False
        if new_record:
            self.data[uid][self._game][self._max_str] = score
            self.save()
        return GameScore(old_max, success=None if old_max is None else new_record)

    def _scoring(self, ctx: commands.Context) -> GameScore:
        """Get the score based on user's input"""
        # This sample function sets an RNG seed to prevent any sort of manipulation based on other cogs
        # ... i don't actually know the random seeds would carry over from other cogs but i don't want to find out!
        random.seed(dt.datetime.now())
        return GameScore(0)

    def _process(self, ctx: commands.Context) -> dict:
        """Meta function for preparing the scoring dict"""
        score_obj = self._scoring(ctx)
        obj = {'score': score_obj}

        # basically, if it didn't fail, test if score/range was a new max
        if score_obj.success is None or score_obj.success:
            for key, item in self._top_keys.items():  # ...for user/guild/global
                obj[key] = self._save_score(eval(item), score_obj.max)
        return obj

    def _add_field(self, ctx: commands.Context, embed: discord.Embed, add_for: str, score_data: GameScore,
                   record_type: str = None) -> discord.Embed:
        if record_type is None:
            record_type = self._max_str
        if score_data.success:  # if user score was a new record compared to old score
            embed.add_field(name=self._localizer.localize(f"{add_for}_{record_type}_name", ctx=ctx),
                            value=self._localizer.localize(f"{self._game}_value_{add_for}_{record_type}", ctx=ctx)
                            .format(score_data.score))
        return embed

    def _embed_process(self, ctx: commands.Context, key: str, item: GameScore, embed: discord.Embed) -> discord.Embed:
        """Adds fields for new bests to an embed"""
        return self._add_field(ctx, embed, key, item)

    def _description(self, ctx: commands.Context, item: GameScore) -> str:
        """Returns description for an embed."""
        author = ctx.author
        suffix = '_'+str(item.success) if item.success is not None else ''
        return self._localizer.localize(self._game + '_description'+suffix, ctx=ctx).format(item, author)

    def _input_parser(self, ctx: commands.Context) -> typing.Union[None, discord.Embed]:
        """Returns a string if input is invalid, otherwise returns None. Should be overridden."""
        return None

    def process(self, ctx: commands.Context) -> discord.Embed:
        """Generates a score and returns the output."""
        errors = self._input_parser(ctx)
        if errors is not None:
            return errors

        embed = embed_author_template(ctx)
        obj = self._process(ctx)
        score = obj['score']
        del obj['score']  # prevent iterating on the score later
        embed.description = self._description(ctx, score)

        not_failure = score.success is None or score.success
        if not_failure:
            for key, item in obj.items():
                embed = self._embed_process(ctx, key, item, embed)

        colour_index = int(not_failure)+len(embed.fields)
        colour = self._colours[colour_index]
        if colour is not None:
            embed.colour = colour
        return embed

    def top(self, ctx: commands.Context, sort_flags: dict = None) -> discord.Embed:
        default_flags = {self._max_str: {'reverse': True}}
        if sort_flags is not None:
            for flag_key, flags in sort_flags:
                default_flags[flag_key] = flags

        errors = self._input_parser(ctx)
        if errors is not None:
            return errors

        # output string
        out_msg = self._localizer.localize('top_msg', ctx=ctx)
        suffix = '_top_suffix'
        add_key = self._game
        if add_key+suffix not in self._localizer.keys:
            add_key = 'generic'
        out_msg += self._localizer.localize(add_key+suffix, ctx=ctx)

        embed = embed_author_template(ctx, subtle_author=True)
        embed.title = ctx.prefix+ctx.command.parent.name+' '+ctx.command.name
        global_key = eval(self._top_keys[self._global_key])
        records = {x: y[self._game] for x, y in self.data.items() if x != global_key and self._game in y}
        vals = list(records.values())
        if not vals:
            embed.description = self._localizer.localize('no_leaderboard', ctx=ctx)
            return embed
        for key in vals[0]:
            key_records = ({'user_id': x, 'score': y[key]} for x, y in records.items() if y[key] is not None)
            flags = default_flags[key] if key in default_flags else {}
            sorted_records = sorted(key_records, key=lambda x: x['score'], **flags)

            placement, prev_record, formatted_records, author_added, ellipses = None, None, [], False, False
            for i, record in enumerate(sorted_records, 1):
                if record['score'] != prev_record:
                    placement = i
                is_author = str(ctx.author.id) == record['user_id']
                # we want to add a '...' to show the gap between author and top 10 if applicable (ie not 11th place)
                if author_added and placement > 10:
                    break
                elif is_author:
                    author_added = True
                elif placement > 10 and not author_added:
                    if not ellipses:
                        formatted_records.append('...')
                        ellipses = True
                    continue
                record['count'] = placement
                member = ctx.bot.get_user(int(record['user_id']))
                record['member'] = member if member is not None else record['user_id']
                formatted_records.append(out_msg.format(record).strip())
                # TODO: "and [x] more..."
            embed.add_field(name=self._localizer.localize(key+'_top_name', ctx=ctx), value='\n'.join(formatted_records))
        return embed

    def import_data(self, old_data_path: str):
        for filename in (f for f in os.listdir(old_data_path)):
            filename_key = filename.replace('.txt', '') if re.match(r"\d{17,}\.txt", filename) else 'global'
            self.init_data(filename_key)
            uservalue = self.data[filename_key][self._game][self._max_str]
            with open(os.path.join(old_data_path, filename), 'r') as f:
                record = f.readlines()[int(filename_key == 'global')].strip()
                try:
                    record = int(record)
                except ValueError:
                    record = float(record)
                if uservalue is not None and not(record > uservalue):
                        continue
                self.data[filename_key][self._game][self._max_str] = record
        self.save()


class Lowderboard(Leaderboard):
    __slots__ = {"_low_str"}

    """Base class for Leaderboards that also track lowest scores"""
    def __init__(self, game, localizer):
        super().__init__(game, localizer)
        self._low_str = 'low'
        self._board_str = [self._max_str, self._low_str]

    def _save_score(self, snowflake: typing.Union[int, str], score: typing.Union[float, int]) -> tuple:
        gs_max = super()._save_score(snowflake, score)
        uid = str(snowflake)
        self.init_data(uid)
        old_min = self.data[uid][self._game][self._low_str]
        # grr this code repeats a little...
        new_min = score
        new_record = True
        if old_min is not None and old_min <= score:
                new_min = old_min
                new_record = False
        self.data[uid][self._game][self._low_str] = new_min
        self.save()
        return gs_max, GameScore(old_min, success=None if old_min is None else new_record)

    def _embed_process(self, ctx, key: str, item: tuple, embed: discord.Embed) -> discord.Embed:
        old_max, old_min = item
        for old, record_for in [(old_max, self._max_str), (old_min, self._low_str)]:
            embed = self._add_field(ctx, embed=embed, add_for=key, score_data=old, record_type=record_for)
        return embed


class PointsLeaderboard(Lowderboard):
    def __init__(self, localizer):
        super().__init__('points', localizer)

    def _scoring(self, ctx: commands.Context) -> GameScore:
        super()._scoring(ctx)
        return GameScore(np.random.pareto(1))


class GuessLeaderboard(Leaderboard):
    __slots__ = {"_length_limit"}

    def __init__(self, localizer):
        super().__init__('guess', localizer)
        self._length_limit = 100

    def _scoring(self, ctx: commands.Context) -> GameScore:
        super()._scoring(ctx)
        guess, max_guess = unpack_args(ctx)
        toguess = random.randint(1, max_guess)
        return GameScore(toguess, success=toguess == guess, max_int=max_guess, user=guess)

    def _input_parser(self, ctx: commands.Context) -> typing.Union[None, discord.Embed]:
        error = super()._input_parser(ctx)
        exargs = unpack_args(ctx)
        if error is None and ctx.command.name != top_cmd:
            guess, max_guess = unpack_args(ctx)
            if max_guess <= 1:
                error = 'max_guess_too_low'
            elif guess < 1:
                error = 'guess_too_low'
            elif guess > max_guess:
                error = 'guess_too_large'
            elif max(map(len, map(str, [guess, max_guess]))) > self._length_limit:
                error = 'number_too_long'
                exargs += (self._length_limit,)
        if error is not None:
            raise UserInputError((self._localizer.localize('error', ctx=ctx) +
                                  self._localizer.localize(self._game+'_error_'+error, ctx=ctx)).format(*exargs))
        return None


class FloatGuessLeaderboard(Leaderboard):
    __slots__ = {"_length_limit"}

    def __init__(self, localizer):
        super().__init__('float_guess', localizer)
        self._length_limit = 22

    def _scoring(self, ctx: commands.Context) -> GameScore:
        super()._scoring(ctx)
        guess = unpack_args(ctx)[0]
        toguess = random.random()
        return GameScore(toguess, success=toguess == guess, max_int=1, user=guess)

    def _input_parser(self, ctx: commands.Context) -> typing.Union[None, discord.Embed]:
        error = super()._input_parser(ctx)
        exargs = unpack_args(ctx)
        if error is None and ctx.command.name != top_cmd:
            guess = unpack_args(ctx)[0]
            if not (0 <= guess < 1):
                error = 'range'
            elif max(map(len, map(str, [guess]))) > self._length_limit:
                error = 'number_too_long'
                exargs += (self._length_limit-2,)
        if error is not None:
            raise UserInputError((self._localizer.localize('error', ctx=ctx) +
                                  self._localizer.localize(self._game+'_error_'+error, ctx=ctx)).format(*exargs))
        return None

    def _add_field(self, ctx: commands.Context, embed: discord.Embed, add_for: str, score_data: GameScore,
                   record_type: str = None) -> discord.Embed:
        return embed  # doesn't make sense for float guess to have bests, so just ignore it


class LeaderboardGames(commands.Cog, name='Leaderboard Games'):
    def __init__(self, bot: Semicolon):
        self.bot = bot
        self.point_leaderboard = PointsLeaderboard(self.bot.i18n)
        self.guess_leaderboard = GuessLeaderboard(self.bot.i18n)
        self.float_leaderboard = FloatGuessLeaderboard(self.bot.i18n)

    @commands.group(invoke_without_command=True, aliases=['battle'])
    @commands.cooldown(rate=2, per=1, type=commands.BucketType.guild)
    @commands.bot_has_permissions(embed_links=True)
    async def points(self, ctx: commands.Context):
        """Generates a random score."""
        await ctx.send(embed=self.point_leaderboard.process(ctx))

    @points.command(name=top_cmd)
    @commands.bot_has_permissions(embed_links=True)
    async def points_top(self, ctx: commands.Context):
        """Lists the highest scores of ;points"""
        await ctx.send(embed=self.point_leaderboard.top(ctx))

    @points.command(name='import')
    @commands.is_owner()
    async def points_import(self, ctx: commands.Context):
        """Imports old data"""
        self.point_leaderboard.import_data('scores')
        await ctx.send(self.bot.i18n.localize('imported', ctx=ctx))

    @commands.group(invoke_without_command=True, name='guess', aliases=['number'])
    @commands.cooldown(rate=2, per=1, type=commands.BucketType.guild)
    @commands.bot_has_permissions(embed_links=True)
    async def guess_cmd(self, ctx: commands.Context, guess: int, max_guess: int = 10):
        """
        Can you guess what number I'm thinking of?
        Optionally specify max_guess to change the range of numbers to [1, max_guess]
        """
        await ctx.send(embed=self.guess_leaderboard.process(ctx))

    @guess_cmd.command(name=top_cmd)
    @commands.bot_has_permissions(embed_links=True)
    async def guess_top(self, ctx: commands.Context):
        """Lists the highest scores of ;guess"""
        await ctx.send(embed=self.guess_leaderboard.top(ctx))

    @guess_cmd.command(name='import')
    @commands.is_owner()
    async def guess_import(self, ctx: commands.Context):
        """Imports old data"""
        self.guess_leaderboard.import_data('numbers')
        await ctx.send(self.bot.i18n.localize('imported', ctx=ctx))

    @commands.group(invoke_without_command=True, name='float', aliases=['float_guess', 'fguess', 'floatg', 'fg'])
    @commands.cooldown(rate=2, per=1, type=commands.BucketType.guild)
    @commands.bot_has_permissions(embed_links=True)
    async def float_guess(self, ctx: commands.Context, guess: float):
        """
         Can you guess what decimal I'm thinking of? (Hint: Probably not.)
         """
        await ctx.send(embed=self.float_leaderboard.process(ctx))

    @float_guess.command(name=top_cmd)
    @commands.bot_has_permissions(embed_links=True)
    async def float_top(self, ctx: commands.Context):
        """Lists the highest scores of ;points"""
        await ctx.send(embed=self.float_leaderboard.top(ctx))


def setup(bot):
    bot.add_cog(LeaderboardGames(bot))
