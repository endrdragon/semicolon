import asyncio
import json
import typing

import discord
from discord.ext import commands
from discord.utils import escape_markdown as esc_md

from cogs.utils.bot import Semicolon
from cogs.utils.constants import CONSTANTS
from cogs.utils.errors import ExpectedError, SQLError
from cogs.utils.sql import TableManager
from cogs.utils.utils import get_roles_by_prefix, set_role, comma_separator, get_guild_names, \
    line_split, \
    delete_after, private_message, get_constant_emoji, ping_user, get_input, data_path

payload_to_bool = {'sync role add': True, 'sync role remove': False}
bool_to_payload = {True: 'sync role add', False: 'sync role remove'}
messages = {}


def get_pronoun(roles: typing.List[discord.Role] = None, guild: discord.Guild = None,
                prefix: str = None, pronoun: str = None):
    pronoun = pronoun.lower()  # this should be done by the time we get here but just to be safe
    if roles is None:
        roles = guild.roles
    return discord.utils.find(lambda x: x.name.startswith(prefix) and x.name.replace(prefix, '', 1).lower() == pronoun,
                              roles)


class Pronouns(commands.Cog):
    def __init__(self, bot: Semicolon):
        self.bot = bot
        self.pronoun_db = TableManager(self.bot.sql, 'pronouns', ['user_id integer PRIMARY KEY', 'pronouns string NOT NULL'])
        self.locks_db = TableManager(self.bot.sql, 'pronoun_locks', ['user_id integer PRIMARY KEY', 'locked_guilds string NOT NULL'])
        self.c_lock = asyncio.Lock()  # prevent race-conditions in the auto role creation of role_process

        self.bot.register_consumer("pronoun listener", self.consumer_handler)

    async def write_database(self, user_id: int, pronoun: str, add_pronoun: bool, origin: str):
        """
        Writes a pronoun addition/removal to the database.
        :param user_id: the requester's ID
        :param pronoun: the string to save
        :param add_pronoun: whether to add (True) or remove (False) the pronoun
        :param origin: the function calling this command -- expected to be space-separated with the origin guild ID
        """

        try:
            user_pronouns = json.loads(self.pronoun_db.get_row_by({'user_id': user_id}, cols = ['pronouns'])[0])
        except SQLError:
            user_pronouns = []
            self.pronoun_db.create_row((user_id, json.dumps(user_pronouns)), ['user_id', 'pronouns'])

        if add_pronoun:
            if pronoun not in user_pronouns:
                user_pronouns.append(pronoun)
                self.pronoun_db.edit_row_by({'user_id': user_id}, (json.dumps(user_pronouns),), ['pronouns'])
        else:
            if pronoun in user_pronouns:
                user_pronouns.remove(pronoun)

            if user_pronouns:
                self.pronoun_db.edit_row_by({'user_id': user_id}, (json.dumps(user_pronouns),), ['pronouns'])
            else:
                # user has no pronoun roles; remove from the database
                self.pronoun_db.delete_row_by({'user_id': user_id})

        # logging...
        log_msg = f"{'Added' if add_pronoun else 'Removed'} <@{user_id}>'s pronoun `{pronoun}` from `{origin}`"
        self.bot.logger.info(log_msg)

        if origin == 'logbote':
            return  # don't want to send data back to logbote...

        orig_split = origin.split(' ')
        data = {'user': user_id,
                'guild': int(orig_split[1]) if len(orig_split) > 1 else None,
                'action': bool_to_payload[add_pronoun],
                'named': pronoun}
        if self.bot.connect_to_lgbt:
            while self.bot.websocket is None or self.bot.websocket.closed:
                await asyncio.sleep(1)
            await self.bot.websocket.send(json.dumps(data))

    async def consumer_handler(self, msg: typing.Dict[str, typing.Any]):
        if msg['action'] not in payload_to_bool:
            return
        await self.write_database(int(msg['user']), msg['named'].lower(), payload_to_bool[msg['action']], 'logbote')
        await self.member_update(msg['named'].lower(), int(msg['user']), payload_to_bool[msg['action']])
        self.bot.logger.debug(f"Saved incoming `{msg['action']}` `{msg['named']}` for <@{msg['user']}>")

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        guild = member.guild
        # add member pronouns on join
        if not guild.me.guild_permissions.manage_roles:
            return
        # save time on role getting and avoid non-pronoun servers
        prefix = self.bot.bot_config['pronoun_prefix']
        roles = get_roles_by_prefix(prefix, guild)
        if not roles:
            return

        for pronoun in self.get_pronouns(member.id):
            rname = prefix + pronoun
            pronoun_role = get_pronoun(roles=roles, prefix=prefix, pronoun=pronoun)
            if pronoun_role is None:
                if pronoun not in CONSTANTS['pronoun_whitelist']:
                    continue
                rs = self.bot.i18n.localize('pronoun_reason', guild=guild)
                pronoun_role = await guild.create_role(name=rname, reason=rs)
            if pronoun_role >= guild.me.top_role:
                continue
            await member.add_roles(pronoun_role, reason=self.bot.i18n.localize('pronoun_reason', guild=guild))

    @commands.Cog.listener()
    async def on_guild_join(self, guild: discord.Guild):
        roles = get_roles_by_prefix(self.bot.bot_config['pronoun_prefix'], guild)
        if not roles:
            return
        for role in guild.roles:
            await self.role_process(role, guild)

    @commands.Cog.listener()
    async def on_guild_role_create(self, role: discord.Role):
        await self.role_process(role, role.guild)

    @commands.Cog.listener()
    async def on_guild_role_update(self, before: discord.Role, after: discord.Role):
        await self.role_process(after, after.guild)

    async def logbote_process(self, data: dict):
        """
        Processes pronoun changes from Dunkel's logbote
        :param data: dict (from json)
        """
        user = data['user']
        guild = data['guild']
        action = data['action']
        pronoun = data['named']

        if action not in payload_to_bool:
            raise NotImplementedError("Supplied action is not implemented")
        add_role = payload_to_bool[action]

        if self.is_locked(int(user), guild):
            return
        await self.write_database(int(user), pronoun, add_role, 'logbote')
        # if async can't be used here, i can instead do smth like:
        # self.to_process = {}
        # self.to_process[utc timestamp + random.randint(1,10000)] = [pronoun, user, add_role]
        # and then create a task that scans the dict every second and updates members accordingly
        await self.member_update(pronoun, int(user), add_role)

    def get_locks(self, user_id: int):
        try:
            return json.loads(self.locks_db.get_row_by({'user_id': user_id}, cols=['locked_guilds'])[0])
        except SQLError:
            return []

    def get_pronouns(self, user_id: int):
        try:
            return json.loads(self.pronoun_db.get_row_by({'user_id': user_id}, cols=['pronouns'])[0])
        except SQLError:
            return []

    def is_locked(self, user_id: int, guild_id: int):
        return guild_id in self.get_locks(user_id)

    def toggle_lock(self, user_id: int, guild_id: int):
        locks = self.get_locks(user_id)
        if guild_id in locks:
            locks.remove(guild_id)
            if locks:
                # we know the row exists here
                self.locks_db.edit_row_by({'user_id': user_id}, (json.dumps(locks),), ['locked_guilds'])
            else:
                self.locks_db.delete_row_by({'user_id': user_id})
            return False
        else:
            locks.append(guild_id)
            locks_json = json.dumps(locks)
            try:
                self.locks_db.edit_row_by({'user_id': user_id}, (locks_json,), ['locked_guilds'])
            except SQLError:
                self.locks_db.create_row((user_id, locks_json), ['user_id', 'locked_guilds'])
            return True

    async def member_update(self, pronoun: str, user_id: int, add_role: bool, guilds: typing.List[discord.Guild] = None,
                            # ctx optional, is used for i18n
                            ctx: commands.Context = None)\
            -> typing.Dict[str, typing.Union[typing.List[discord.Guild], typing.Dict[discord.Guild, str]]]:  # lol

        data = {"successes": [], "failures": {}}

        def process_output(server: discord.Guild, status=None):
            if status is not None:
                data['failures'][server] = status
            else:
                data['successes'].append(server)

        is_list = guilds is not None
        if not is_list:
            guilds = self.bot.guilds
        for guild in guilds:
            user = guild.get_member(user_id)
            if user is None:
                continue

            if not is_list and self.is_locked(user_id, guild.id):
                process_output(guild, self.bot.i18n.localize('pronoun_failure_locked', ctx=ctx, user=user))
                continue

            proles = get_roles_by_prefix(self.bot.bot_config['pronoun_prefix'], guild)
            if not proles:
                # not a semicolon pronoun server, ignore it.
                continue

            if not guild.me.guild_permissions.manage_roles:
                process_output(guild, self.bot.i18n.localize('pronoun_failure_no_perm', ctx=ctx, user=user))
                continue

            rname = self.bot.bot_config['pronoun_prefix'] + pronoun
            pronoun_role = get_pronoun(proles, prefix=self.bot.bot_config['pronoun_prefix'], pronoun=pronoun)
            if pronoun_role is None:
                if pronoun in CONSTANTS['pronoun_whitelist']:
                    rs = self.bot.i18n.localize('pronoun_reason', guild=guild)
                    pronoun_role = await guild.create_role(name=rname, reason=rs)
                else:
                    process_output(guild, self.bot.i18n.localize('pronoun_failure_no_role', ctx=ctx, user=user))
                    continue
            if pronoun_role >= guild.me.top_role:
                process_output(guild, self.bot.i18n.localize('pronoun_failure_too_high', ctx=ctx, user=user))
                continue
            await set_role(user, pronoun_role, add_role)
            process_output(guild)
        return data

    async def role_process(self, role: discord.Role, guild: discord.Guild, users: typing.List[int] = None):
        prefix = self.bot.bot_config['pronoun_prefix']

        if not role.name.startswith(prefix):
            return
        if not guild.me.guild_permissions.manage_roles:
            return
        if role >= guild.me.top_role:
            return

        for user_id, pronouns_json in self.pronoun_db.get_rows():
            pronouns = json.loads(pronouns_json)

            if users and user_id not in users:
                continue
            if guild.id in self.get_locks(user_id):
                continue
            user = guild.get_member(user_id)
            if user is None:
                continue
            pronoun_name = role.name.replace(prefix, '', 1).lower()
            user_has_pronoun = pronoun_name in pronouns
            await set_role(user, role, user_has_pronoun)

        async with self.c_lock:
            guild = self.bot.get_guild(guild.id)
            gnames = [r.name.replace(prefix, '', 1).lower() for r in get_roles_by_prefix(prefix, guild)]
            for pronoun in CONSTANTS['pronoun_whitelist']:
                if pronoun not in gnames:
                    rname = prefix + pronoun
                    await guild.create_role(name=rname, reason=self.bot.i18n.localize('pronoun_reason', guild=guild))
                    break  # the new role will trigger a role_process and create the next role

    @commands.group(name='pronoun', invoke_without_command=True, case_insensitive=True, aliases=['pronouns'], help=
    "Sets a pronoun role across all participating servers.\n" +
    "Usage examples: `pronoun on he/him`, `pronoun she`, `pronoun disable any`\n" +
    "Moderators can enable the command with `setup pronouns`.\n",
                    usage='[enable/disable] <pronoun>')
    @commands.cooldown(rate=3, per=60, type=commands.BucketType.user)
    async def pronoun_cmd(self, ctx: commands.Context, action: typing.Optional[bool] = None, *, pronoun: str):
        async with ctx.typing():
            is_dm = isinstance(ctx.channel, discord.DMChannel)
            pronoun = pronoun.lower()
            if pronoun == 'pronouns' and not action and action is not None:
                pronoun = 'no pronouns'
                action = None
            for p_allowed in CONSTANTS['pronoun_whitelist']:
                for p_split in p_allowed.split('/') + [p_allowed.split(' ')[0]]:
                    if pronoun == p_split:
                        pronoun = p_allowed

            guild_is_locked = self.is_locked(ctx.author.id, ctx.guild.id) if not is_dm else False

            if action is None:
                if guild_is_locked:
                    user_pronouns = [x.name.replace(self.bot.bot_config['pronoun_prefix'], '', 1).lower() for x
                    in get_roles_by_prefix(self.bot.bot_config['pronoun_prefix'], ctx.author)]
                else:
                    user_pronouns = self.get_pronouns(ctx.author.id)
                action = pronoun not in user_pronouns

            process_guilds = None
            if guild_is_locked:
                process_guilds = [ctx.guild]
            else:
                origin = 'COMMAND' + (f' {ctx.guild.id}' if ctx.guild else '')
                await self.write_database(ctx.author.id, pronoun, action, origin)
            status = await self.member_update(pronoun, ctx.author.id, action, process_guilds)
            successes = status['successes']
            errors = status['failures']

            private_output = []
            public_output = []
            delete_delay = 15
            i18n_key = 'added' if action else 'removed'
            smoji = CONSTANTS['emojis']['SUCCESS'][0]
            fmoji = CONSTANTS['emojis']['FAILURE'][0]

            if len(successes) == 0 and len(errors) == 0:
                public_output.append(self.bot.i18n.localize('pronoun_nothing', ctx=ctx))
            elif len(successes) == 0 and len(errors) == 1 and list(errors.keys())[0] == ctx.guild:
                # display error if only the current guild was changed
                public_output.append(list(errors.values())[0])
                delete_delay = None
            elif len(successes) == 1 and len(errors) == 0 and successes[0] == ctx.guild:
                # avoid DMing if only the current guild was affected
                cs = comma_separator([f'`{esc_md(guild.name)}`' for guild in successes])
                ostr = self.bot.i18n.localize_p('pronoun_dm_successes_' + i18n_key, len(successes), ctx=ctx)
                public_output.append(ostr.format('', pronoun, cs).strip())
            else:
                for status, entries in status.items():
                    if not entries:
                        continue
                    cs = get_guild_names(entries)
                    emoji = smoji if status == 'successes' else fmoji
                    pu_str = self.bot.i18n.localize_p(f'pronoun_{status}_{i18n_key}', len(entries), ctx=ctx)
                    pr_str = self.bot.i18n.localize_p(f'pronoun_dm_{status}_{i18n_key}', len(entries), ctx=ctx)
                    public_output.append(pu_str.format(pronoun, len(entries)))
                    private_output.append(pr_str.format(emoji, pronoun, cs))
                if len(errors) > 0:
                    errs = ["**{}**: {}".format(esc_md(guild.name), reason) for guild, reason in errors.items()]
                    private_output += errs

            add_to = private_output if is_dm else public_output
            if not guild_is_locked:
                add_to.append(self.bot.i18n.localize(f'pronoun_db_{i18n_key}', ctx=ctx).format(pronoun))
            else:
                add_to.append(self.bot.i18n.localize(f'pronoun_nodb_{i18n_key}', ctx=ctx).format(pronoun))

            # send output
            if is_dm:
                # no try/except because user DM'd us so we can DM back
                out = line_split('\n'.join(private_output))
                if not out:
                    o = self.bot.i18n.localize("pronoun_no_output", ctx=ctx)
                    await ctx.author.send(o)
                    raise NotImplementedError(o)
                for msg in out:
                    await ctx.author.send(msg)
                return

            if private_output:
                pub_msg = self.bot.i18n.localize('pronoun_read_dms', ctx=ctx)
                try:
                    for msg in line_split('\n'.join(private_output)):
                        await ctx.author.send(msg)
                except discord.Forbidden:
                    pub_msg = self.bot.i18n.localize('pronoun_enable_dms', ctx=ctx)
                public_output.append(pub_msg)

            public_output[0] = ctx.author.mention + ': ' + public_output[0]
            msgs = line_split('\n'.join(public_output))
            await ctx.reply(msgs[0], delete_after=delete_delay, mention_author=False)
            for msg in msgs[1:]:
                await ctx.send(msg, delete_after=delete_delay)  # allowed_mentions intentionally left out
            await delete_after(ctx.message, delete_delay)

    @pronoun_cmd.command(name='help')
    async def p_help(self, ctx: commands.Context):
        await ctx.send_help(ctx.command.parent)

    @pronoun_cmd.command(name='database')
    async def p_database(self, ctx: commands.Context):
        """Sends you your current pronouns and locked servers"""
        pronouns = self.get_pronouns(ctx.author.id)
        locks = []
        for gid in self.get_locks(ctx.author.id):
            guild = self.bot.get_guild(gid)
            gname = guild.name if guild else self.bot.i18n.localize('pronoun_lock_unknown', ctx=ctx).format(gid)
            locks.append(gname)

        if pronouns:
            cs = comma_separator([f"`{x}`" for x in pronouns])
            pout = self.bot.i18n.localize_p('pronoun_list', len(pronouns), ctx=ctx).format(cs)
        else:
            pout = self.bot.i18n.localize('pronoun_no_list', ctx=ctx)

        if locks:
            cs = comma_separator([f"`{x}`" for x in locks])
            lout = self.bot.i18n.localize_p('pronoun_lock_list', len(locks), ctx=ctx).format(cs)
        else:
            lout = self.bot.i18n.localize('pronoun_no_lock', ctx=ctx)

        await private_message(ctx, '\n'.join([pout, lout]))

    async def guild_import(self, ctx):
        # get current pronouns
        pronouns = []
        for role in ctx.author.roles:
            rolename = role.name  # .lower()
            if rolename.startswith(self.bot.bot_config['pronoun_prefix']):
                pronouns.append(rolename.replace(self.bot.bot_config['pronoun_prefix'], '', 1))

        # remove old pronouns
        current_pronouns = self.get_pronouns(ctx.author.id)
        for pronoun in current_pronouns:
            if pronoun in pronouns:
                continue
            await self.write_database(ctx.author.id, pronoun, False, f'IMPORT {ctx.guild.id}')
            await self.member_update(pronoun, ctx.author.id, False)

        # add new pronouns
        for pronoun in pronouns:
            if pronoun in current_pronouns:
                continue
            await self.write_database(ctx.author.id, pronoun, True, f'IMPORT {ctx.guild.id}')
            await self.member_update(pronoun, ctx.author.id, True)

    async def db_import(self, ctx):
        success = {"add": 0, "rem": 0}
        errors = {"rem_high": [], "nonexistent": [], "add_high": []}

        pronouns = self.get_pronouns(ctx.author.id)
        uroles = {x: x.name.replace(self.bot.bot_config['pronoun_prefix'], '', 1).lower() for x
                  in get_roles_by_prefix(self.bot.bot_config['pronoun_prefix'], ctx.author)}

        # remove pronouns not in db
        remroles = []
        for prole, pname in uroles.items():
            if pname not in pronouns:
                if prole > ctx.guild.me.top_role:
                    errors['rem_high'].append(pname)
                else:
                    success['rem'] += 1
                    remroles.append(prole)
        if remroles:
            await ctx.author.remove_roles(*remroles, reason=self.bot.i18n.localize('pronoun_reason', guild=ctx.guild))

        # add pronouns not in db
        addroles = []
        for pronoun in pronouns:
            if pronoun not in uroles.values():
                prolename = self.bot.bot_config['pronoun_prefix'] + pronoun
                prole = get_pronoun(guild=ctx.guild, prefix=self.bot.bot_config['pronoun_prefix'], pronoun=pronoun)
                if prole is None:
                    if pronoun in CONSTANTS['pronoun_whitelist']:
                        newrole = await ctx.guild.create_role(name=prolename)
                        success['add'] += 1
                        addroles.append(newrole)
                    else:
                        errors['nonexistent'].append(pronoun)
                elif prole > ctx.guild.me.top_role:
                    errors['add_high'].append(pronoun)
                else:
                    success['add'] += 1
                    addroles.append(prole)
        if addroles:
            await ctx.author.add_roles(*addroles, reason=self.bot.i18n.localize('pronoun_reason', guild=ctx.guild))

        return success, errors

    @pronoun_cmd.command(name='g-import', brief='Replaces your saved pronouns with pronouns from the current server')
    @commands.guild_only()
    @commands.cooldown(rate=1, per=60, type=commands.BucketType.user)
    async def p_gimport(self, ctx: commands.Context):
        """Imports your pronoun data from the current server into the database, replacing your existing entries.
        This will replace your pronouns on all unlocked servers.
        Runs regardless of whether your current server is locked or not."""
        await self.guild_import(ctx)
        await ctx.send(self.bot.i18n.localize('pronoun_gimport', ctx=ctx).format(ctx.author.mention),
                       allowed_mentions=ping_user(ctx))

    @pronoun_cmd.command(name='db-import')
    @commands.guild_only()
    @commands.cooldown(rate=1, per=60, type=commands.BucketType.user)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def p_dbimport(self, ctx):
        """Replaces your current pronoun roles with your saved pronouns."""
        success, errors = await self.db_import(ctx)
        output = []
        smoji = get_constant_emoji('SUCCESS', ctx)
        fmoji = get_constant_emoji('FAILURE', ctx)
        for skey, count in success.items():
            if count > 0:
                o = self.bot.i18n.localize_p(f"pronoun_dbimport_success_{skey}", count, ctx=ctx).format(smoji, count)
                output.append(o)
        for fkey, pronouns in errors.items():
            for pronoun in pronouns:
                o = self.bot.i18n.localize(f"pronoun_dbimport_{fkey}", ctx=ctx).format(fmoji, pronoun)
                output.append(o)
        if not output:
            output = [self.bot.i18n.localize(f"pronoun_dbimport_nothing", ctx=ctx)]
        await ctx.send('\n'.join(output))

    @pronoun_cmd.command(name='lock')
    @commands.guild_only()
    @commands.bot_has_guild_permissions(manage_roles=True, embed_links=True)
    @commands.cooldown(rate=2, per=60, type=commands.BucketType.user)
    async def p_lock(self, ctx: commands.Context):
        """Prevents your pronouns from being changed in the current server.
        When trying to change pronouns in a locked server, it will only change your roles in that server.
        Used for if you're out to some server but not others."""
        to_add = not self.is_locked(ctx.author.id, ctx.guild.id)
        cmd = ctx.prefix + self.p_lock.qualified_name
        if to_add:
            self.toggle_lock(ctx.author.id, ctx.guild.id)
            await ctx.send(self.bot.i18n.localize("pronoun_lock", ctx=ctx).format(ctx.author.mention, cmd),
                           allowed_mentions=ping_user(ctx), delete_after=15)
        else:
            choice = await get_input(title=ctx.bot.i18n.localize('pronoun_lock_input_title', ctx=ctx),
                                     prompt=ctx.bot.i18n.localize('pronoun_lock_input_desc', ctx=ctx).format(
                                         ctx.author.mention),
                                     items=[ctx.bot.i18n.localize('pronoun_lock_input_1', ctx=ctx),
                                            ctx.bot.i18n.localize('pronoun_lock_input_2', ctx=ctx)],
                                     selections=[True, False], ctx=ctx)
            if choice is None:
                return
            self.toggle_lock(ctx.author.id, ctx.guild.id)
            if choice:  # save roles into database
                await self.guild_import(ctx)
            else:  # save database into roles
                await self.db_import(ctx)
            await ctx.send(self.bot.i18n.localize("pronoun_unlock", ctx=ctx).format(ctx.author.mention, cmd),
                           allowed_mentions=ping_user(ctx), delete_after=15)
        await delete_after(ctx.message, 15)

    @pronoun_cmd.command(name='setup')
    @commands.guild_only()
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def p_setup(self, ctx: commands.Context):
        """Sets up pronouns on the server"""
        message = ctx.message
        message.content = f"{ctx.prefix}setup pronouns"
        await self.bot.process_commands(message)

    @pronoun_cmd.command(name='add', aliases=['create'])
    @commands.guild_only()
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def p_add(self, ctx: commands.Context, *, pronoun: str):
        """Adds a pronoun to the server"""
        if len(ctx.guild.roles) >= 250:  # max role limit
            raise ExpectedError(ctx.bot.i18n.localize('too_many_roles', ctx=ctx))
        pronoun = pronoun.lower()
        rname = self.bot.bot_config['pronoun_prefix'] + pronoun
        if get_pronoun(guild=ctx.guild, prefix=self.bot.bot_config['pronoun_prefix'], pronoun=pronoun):
            await ctx.send(self.bot.i18n.localize('pronoun_add_exists', ctx=ctx))
            return
        await ctx.guild.create_role(name=rname,
                                    reason=self.bot.i18n.localize('reason', guild=ctx.guild).format(
                                        ctx.author))
        await ctx.send(self.bot.i18n.localize('pronoun_add_success', ctx=ctx).format(rname))

    @pronoun_cmd.command(name='remove', aliases=['delete'])
    @commands.guild_only()
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def p_remove(self, ctx: commands.Context, *, pronoun: str):
        """Removes a pronoun from the server"""
        pronoun = pronoun.lower()
        if pronoun in CONSTANTS['pronoun_whitelist']:
            await ctx.send(self.bot.i18n.localize('pronoun_remove_whitelisted', ctx=ctx))
            return

        rname = self.bot.bot_config['pronoun_prefix'] + pronoun
        role = get_pronoun(guild=ctx.guild, prefix=self.bot.bot_config['pronoun_prefix'], pronoun=pronoun)
        if not role:
            await ctx.send(self.bot.i18n.localize('pronoun_remove_nonexists', ctx=ctx))
            return
        if role > ctx.guild.me.top_role:
            await ctx.send(self.bot.i18n.localize('pronoun_remove_too_high', ctx=ctx))
            return

        await role.delete(reason=self.bot.i18n.localize('reason', guild=ctx.guild).format(ctx.author))
        await ctx.send(self.bot.i18n.localize('pronoun_remove_success', ctx=ctx).format(rname))

    @pronoun_cmd.command(name='whitelist')
    async def p_whitelist(self, ctx):
        """Posts the current list of pronouns the bot will create."""
        whitelist = comma_separator([f'`{x}`' for x in CONSTANTS['pronoun_whitelist']])
        await ctx.send(self.bot.i18n.localize('pronoun_whitelist', ctx=ctx).format(ctx.bot.user.mention, whitelist))

    @pronoun_cmd.command(name='list')
    @commands.guild_only()
    async def p_list(self, ctx: commands.Context):
        """Posts the list of pronouns on the current server."""
        roles = [x.name.replace(self.bot.bot_config['pronoun_prefix'], '', 1).lower() for x
                 in get_roles_by_prefix(self.bot.bot_config['pronoun_prefix'], ctx.guild)]
        if not roles:
            await ctx.send(self.bot.i18n.localize('pronoun_guild_no_list', ctx=ctx))
            return
        rlower = [x.lower() for x in roles]
        for pronoun in CONSTANTS['pronoun_whitelist']:
            if pronoun.lower() not in rlower:
                roles.append(pronoun)
        roles = [f"`{x}`" for x in roles]
        await ctx.send(self.bot.i18n.localize('pronoun_guild_list', ctx=ctx).format(comma_separator(roles)))

    @pronoun_cmd.command(name='legacy-import')
    @commands.is_owner()
    async def p_legacy_import(self, ctx: commands.Context, overwrite: bool = False):
        """Imports pronouns from the legacy database"""
        with open(data_path('pronouns.json'), 'r') as f:
            data = json.load(f)
            for user_id_str, value in data.items():
                if user_id_str == 'locks':
                    for locked_user_id_str, locked_guild_ids in value.items():
                        if overwrite:
                            self.locks_db.delete_row_by({"user_id": int(locked_user_id_str)})
                        self.locks_db.create_row((int(locked_user_id_str), json.dumps(locked_guild_ids)), ['user_id', 'locked_guilds'])
                else:
                    if overwrite:
                        self.pronoun_db.delete_row_by({"user_id": int(user_id_str)})
                    self.pronoun_db.create_row((int(user_id_str), json.dumps(value)), ['user_id', 'pronouns'])
        await ctx.send(get_constant_emoji('SUCCESS', ctx))


def setup(bot):
    bot.add_cog(Pronouns(bot))
