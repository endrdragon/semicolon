from discord.ext import commands
from discord.utils import escape_markdown as emd

import cogs.utils.utils as utils
from cogs.utils.bot import Semicolon


def is_mod(ctx):
    return ctx.author.id in [130744447930597377, 140564059417346049, 136611352692129792, 197876594755371008]


class PSUB(commands.Cog):
    def __init__(self, bot: Semicolon):
        self.bot = bot
        self._ = bot.i18n.localize
        self.manager = utils.JsonManager('psub.json')
        self.users = ["8thkingdom", "8uny", "96lb", "amaxonite", "ame", "americatress", "andyman", "azura", "azurite",
                      "bazinga_9000", "bum", "cobaltforever", "coohaa", "connie", "conspicuouspiggy", "crafty7",
                      "diamondcup67", "doggo", "dunkelblau", "endr", "fluffy_boi", "geobica", "ahkvenir", "gradedaplus",
                      "greentree", "hanss314", "helen", "imagine4", "iyxon", "izzy", "jenningsasyetuntitled",
                      "josephhoward", "jules", "kaprekar", "kero", "lexikiq", "lifeofchrome", "lock", "lorentzel",
                      "lpcarver", "mcoddzen", "melodystarzxd", "milojacquet", "nuthatch", "opkos", "pmp", "pokey",
                      "polodo", "purplegaze", "qwerbyking", "sandblast", "satdsukhi", "sapphisylph", "sean_",
                      "sergeantsnivy", "sheñicicle", "slice", "som1sezhi", "spitch", "spicyman33", "swimswum",
                      "tantusar", "tautologicaloxymoron", "thatperson", "theepicjames", "thenamesh", "tonywang",
                      "tophatthehat", "wooningc", "yessoan"]
        self.users = sorted(self.users)
        self.data = self.manager.data
        for u in self.users:
            self.manager.init_data(u, {})
            if 'comments' not in self.data[u]:
                self.data[u]['comments'] = {}
            if 'design' not in self.data[u]:
                self.data[u]['design'] = {}

    def get_by_key(self, user: str, key: str):
        if user not in self.data:
            return None
        return self.data[user][key]

    def get_comments(self, user: str):
        return self.get_by_key(user, 'comments')

    def get_designs(self, user: str):
        return self.get_by_key(user, 'design')

    async def submit(self, submit_to: str, ctx: commands.Context, user: str, message: str):
        # psub has closed
        # out = self._('psub_log_attempt').format(ctx.author, submit_to, user) + '\n>>> ' + utils.shorten(message, 1996)
        # self.bot.logger.info(out)
        # await ctx.send(self._('psub_closed', ctx=ctx))
        # return

        comments = self.get_by_key(user, submit_to)
        if comments is None:
            await ctx.send(self._('psub_no_user', ctx=ctx).format(ctx.prefix))
            return
        dkey = str(ctx.author.id)
        old_comment = comments[dkey] if dkey in comments else None
        comments[dkey] = message
        self.manager.save()
        embed = utils.embed_author_template(ctx, True)
        embed.title = self._(f'psub_title_{submit_to}', ctx=ctx)
        embed.description = self._('psub_desc', ctx=ctx).format(user, ctx.prefix)
        if old_comment:
            embed.add_field(name=self._('psub_field_old', ctx=ctx), value=utils.shorten(old_comment, 1024), inline=False)
        embed.add_field(name=self._('psub_field_new', ctx=ctx), value=utils.shorten(message, 1024), inline=False)

        self.bot.logger.info(self._('psub_log').format(ctx.author, submit_to, user))
        await ctx.send(embed=embed)

    @commands.group(name='psub', invoke_without_command=True, hidden=True)
    @commands.dm_only()
    async def psub_help(self, ctx: commands.Context):
        """
        Anonymously send polite messages to members of the ChiP community. (You can sign your messages if you wish!)
        Messages will not be sent directly but rather via a personalized card/image in a month or so.
        The messages can be left with `;psub submit <username> <comment>`, and suggestions for the card designs (ie how you would describe the aesthetic of a person (favorite colors, motifs, etc)) with `;psub design <username> <comment>`.
        You can override your submissions at any time with the same command.
        You can get your submissions with `;psub get <username>`.
        The list of users can be found with `;psub users`.
        """
        await ctx.send_help(ctx.command)

    @psub_help.command(name='submit')
    @commands.dm_only()
    async def psub_submit(self, ctx: commands.Context, user: str, *, message: str):
        """
        Submit a message.
        """
        await self.submit('comments', ctx, user.lower(), message)

    @psub_help.command(name='design')
    @commands.dm_only()
    async def psub_design(self, ctx: commands.Context, user: str, *, message: str):
        """
        Suggest aesthetics for a user's card design
        """
        await self.submit('design', ctx, user.lower(), message)

    @psub_help.command(name='get')
    @commands.dm_only()
    async def psub_get(self, ctx: commands.Context, user: str):
        """
        Gets your comments for a user.
        """
        comments = self.get_comments(user)
        if comments is None:
            await ctx.send(self._('psub_no_user', ctx=ctx).format(ctx.prefix))
            return
        designs = self.get_designs(user)
        output = []
        dkey = str(ctx.author.id)
        if dkey in comments:
            output.append(self._('psub_list_header', ctx=ctx) + "```{}```".format(comments[dkey]))
        if dkey in designs:
            output.append(self._('psub_list_header2', ctx=ctx) + "```{}```".format(designs[dkey]))
        if output:
            await ctx.send('\n'.join(output))
        else:
            await ctx.send(self._('psub_get_nothing', ctx=ctx))

    @psub_help.command(name='users')
    @commands.dm_only()
    async def psub_users(self, ctx: commands.Context):
        """
        Lists the available users and who you haven't responded to.
        """
        output = {'missing': [], 'comments': [], 'design': [], 'both': []}
        ukey = str(ctx.author.id)

        for user, data in self.data.items():
            commented = False
            if ukey in data['comments'] and ukey in data['design']:
                commented = True
                output['both'].append(user)
            else:
                for key, comments in data.items():
                    if ukey in comments:
                        commented = True
                        output[key].append(user)
            if not commented:
                output['missing'].append(user)

        out_list = []
        for key, users in output.items():
            if not users:
                continue
            out_list.append(self._('psub_users_'+key, ctx=ctx).format(emd(utils.comma_separator(users))))
        await ctx.send('\n'.join(out_list))

    @psub_help.command(name='submissions')
    @commands.dm_only()
    @commands.check(is_mod)
    async def psub_list(self, ctx, *users):
        """
        [psub moderator only] List submissions for users.
        """
        users = list(users)
        if not users:
            users = self.users

        output = []
        for user in users:
            comments = self.get_comments(user)
            if comments is None:
                await ctx.send(self._('psub_no_user', ctx=ctx).format(ctx.prefix))
                return
            designs = self.get_designs(user)
            output_list = []
            if comments:
                output_list.append(self._('psub_list_header', ctx=ctx) + f" ({user})")
                for uid, msg in comments.items():
                    output_list.append("<@%s>" % uid)
                    output_list += ["> %s" % (line if line else '** **') for line in emd(msg).split('\n')]
                    output_list.append('')
            if designs:
                output_list.append(self._('psub_list_header2', ctx=ctx)+f" ({user})")
                for uid, msg in designs.items():
                    output_list.append("<@%s>" % uid)
                    output_list += ["> %s" % (line if line else '** **') for line in emd(msg).split('\n')]
                    output_list.append('')
            output += output_list
        if not output:
            output = [self._('no_output', ctx=ctx)]
        for o in utils.line_split('\n'.join(output)):
            await ctx.send(o)

    @psub_help.command(name='glob-submit')
    @commands.dm_only()
    @commands.is_owner()
    async def psub_glob_submit(self, ctx, userid, *, msg):
        for user in self.users:
            comments = self.get_by_key(user, 'comments')
            if comments is None:
                await ctx.send(self._('psub_no_user', ctx=ctx).format(ctx.prefix))
                return
            comments[userid] = msg
            self.manager.save()
        await ctx.send('\N{THUMBS UP SIGN}')


def setup(bot):
    bot.add_cog(PSUB(bot))
