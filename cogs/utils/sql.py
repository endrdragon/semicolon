import sqlite3
import typing

from .errors import SQLError
from .utils import data_path


def comma_join(t: typing.Iterable):
    return ','.join(map(str, t))


class SQLManager:
    __slots__ = {"db_file", "con"}

    def __init__(self, filename: str):
        self.db_file = data_path(f"{filename}.db")
        self.con = sqlite3.connect(self.db_file)

    def execute(self, command, parameters=None) -> sqlite3.Cursor:
        c = self.con.cursor()
        if parameters:
            c.execute(command, parameters)
        else:
            c.execute(command)
        self.con.commit()
        return c

    def create_table(self, table_name: str, columns: typing.List[str]):
        """
        Column Examples:
        id integer PRIMARY KEY
        name text NOT NULL
        begin_date text
        """
        return self.execute("CREATE TABLE IF NOT EXISTS {} (\n{}\n);".format(table_name, comma_join(columns)))

    def delete_table(self, table_name: str):
        return self.execute("DROP TABLE IF EXISTS {}".format(table_name))

    def create_entry(self, table_name: str, column_names: typing.List[str], data: tuple):
        data = tuple(data)
        if len(column_names) != len(data):
            raise ValueError("Column and data count should match")
        cmd = 'INSERT INTO {}({})\nVALUES({})'.format(table_name, comma_join(column_names), comma_join(['?']*len(data)))
        return self.execute(cmd, data)

    def update_entry(self, table_name: str, cols: typing.List[str], write: typing.Tuple[str], update_by: str,
                     update_id: tuple):
        write = tuple(write)
        update_id = tuple(update_id)
        if len(cols) != len(write):
            raise ValueError("Column and data count should match")
        columns_fmt = (f"{x} = ?" for x in cols)
        cmd = "UPDATE {} SET {} WHERE {}".format(table_name, comma_join(columns_fmt), update_by)
        c = self.execute(cmd, (*write, *update_id))
        if c.rowcount == 0:
            raise SQLError("No rows were affected")
        return c

    def delete_entry(self, table_name: str, delete_by: str, delete_id: tuple):
        delete_id = tuple(delete_id)
        c = self.execute('DELETE FROM {} WHERE {}'.format(table_name, delete_by), delete_id)
        if c.rowcount == 0:
            raise SQLError("No rows were affected")
        return c

    def wipe_table(self, table_name: str):
        return self.execute('DELETE FROM {}'.format(table_name))

    def get_row_by(self, table_name: str, search: str, params: tuple, get_cols: str = None, get_all: bool = False):
        if get_cols is None:
            get_cols = '*'
        c = self.execute('SELECT {} FROM {} WHERE {}'.format(get_cols, table_name, search), params)
        row = c.fetchall() if get_all else c.fetchone()
        if row is None:
            raise SQLError('No data found in {0} - {1} matching {2}'.format(table_name, search, comma_join(params)))
        return row

    def get_all_rows(self, table_name: str, order_by: str = None, get_cols: str = None):
        if get_cols is None:
            get_cols = '*'
        order_by = ' ORDER BY ' + order_by if order_by else ''
        return self.execute('SELECT {} FROM {}{}'.format(get_cols, table_name, order_by)).fetchall()


class TableManager:
    __slots__ = {"sql", "db_file", "con", "name", "col", "col_titles", "col_filtered"}

    def __init__(self, sql: SQLManager, table_name: str, columns: typing.List[str]):
        self.sql = sql
        self.db_file = sql.db_file
        self.con = sql.con
        self.name = table_name
        self.col = columns
        self.col_titles = [x.split(' ')[0] for x in columns]
        self.col_filtered = self.col_titles  # for subclasses to expand on

        self.sql.create_table(self.name, self.col)

    def _get_selector(self, with_rowid: bool = None, columns: typing.List[str] = None) -> str:
        if with_rowid is None and columns is None:
            return None
        cols = columns if columns else ['*']
        if with_rowid:
            cols.insert(0, 'rowid')
        return comma_join(cols)

    def _get_id_by_index(self, index: int, sfilter: dict = None):
        if sfilter is not None:
            return self.get_rows_by(sfilter, with_rowid=True)[index-1][0]
        else:
            return self.get_row(index, with_rowid=True)[0]

    def _where(self, data: dict) -> (str, tuple):
        search = ' AND '.join((f"{x} = ?" for x in data.keys()))
        params = tuple(data.values())
        return search, params

    def delete(self):
        return self.sql.delete_table(self.name)

    def close(self):
        if self.get_rows() is None:
            self.delete()

    def create_row(self, data: tuple, cols: typing.List = None):
        if cols is None:
            cols = self.col_titles
        return self.sql.create_entry(self.name, cols, data)

    def edit_row(self, index: int, data: tuple, cols: typing.List = None, by_rowid: bool = False, sfilter: dict = None):
        if cols is None:
            cols = self.col_filtered
        if not by_rowid:
            index = self._get_id_by_index(index, sfilter)
        if not sfilter:
            sfilter = {}
        sfilter['rowid'] = index
        qsearch, params = self._where(sfilter)
        return self.sql.update_entry(self.name, cols, data, qsearch, params)

    def edit_row_by(self, search: dict, new_data: tuple, cols: typing.List = None):
        search, params = self._where(search)
        if cols is None:
            cols = self.col_filtered
        return self.sql.update_entry(self.name, cols, new_data, search, params)

    def delete_row(self, index: int, by_rowid: bool = False, sfilter: dict = None):
        if not by_rowid:
            index = self._get_id_by_index(index, sfilter)
        if sfilter is None:
            sfilter = {}
        sfilter['rowid'] = index
        search, params = self._where(sfilter)
        return self.sql.delete_entry(self.name, search, params)

    def delete_row_by(self, search: dict):
        search, params = self._where(search)
        return self.sql.delete_entry(self.name, search, params)

    def get_row(self, index: int, with_rowid: bool = False):
        rows = self.get_rows(with_rowid)
        if len(rows) < index:
            raise ValueError("Row doesn't exist")
        return rows[index-1]

    def get_row_by(self, data: dict, with_rowid: bool = False, cols: typing.List[str] = None):
        search, params = self._where(data)
        return self.sql.get_row_by(self.name, search, params, get_cols=self._get_selector(with_rowid, cols))

    def get_rows_by(self, data: dict, with_rowid: bool = False, cols: typing.List[str] = None):
        search, params = self._where(data)
        selector = self._get_selector(with_rowid, cols)
        return self.sql.get_row_by(self.name, search, params, get_cols=selector, get_all=True)

    def get_rows(self, with_rowid: bool = False):
        return self.sql.get_all_rows(self.name, get_cols=self._get_selector(with_rowid))


class GuildManager(TableManager):
    __slots__ = {"snowflake", "title"}

    def __init__(self, sql: SQLManager, table_name: str, columns: typing.List[str], snowflake: int,
                 title: str = 'guild'):
        self.snowflake = snowflake
        self.title = title
        columns.insert(0, self._get_identifier_column())
        super().__init__(sql, table_name, columns)
        self.col_filtered = self.col_filtered[1:]

    def _get_identifier_column(self):
        return f"{self.title} integer NOT NULL"

    def create_row(self, data: tuple, cols: list = None):
        data = (self.snowflake, *data)
        return super().create_row(data, cols)

    def edit_row(self, index: int, data: tuple, cols: typing.List = None, by_rowid: bool = False, sfilter: dict = None):
        if sfilter is None:
            sfilter = {}
        sfilter[self.title] = self.snowflake
        return super().edit_row(index, data, cols, by_rowid, sfilter)

    def edit_row_by(self, search: dict, new_data: tuple, cols: typing.List = None):
        search[self.title] = self.snowflake
        return super().edit_row_by(search, new_data, cols)

    def delete_row(self, index: int, by_rowid: bool = False, sfilter: dict = None):
        if sfilter is None:
            sfilter = {}
        sfilter[self.title] = self.snowflake
        return super().delete_row(index, by_rowid, sfilter)

    def delete_row_by(self, search: dict):
        search[self.title] = self.snowflake
        return super().delete_row_by(search)

    # get_row isn't redefined because it just calls get_rows

    def get_row_by(self, data: dict, with_rowid: bool = False, cols: typing.List[str] = None):
        data[self.title] = self.snowflake
        if cols is None:
            cols = self.col_filtered
        return super().get_row_by(data, with_rowid, cols)

    def get_rows_by(self, data: dict, with_rowid: bool = False, cols: typing.List[str] = None):
        data[self.title] = self.snowflake
        if cols is None:
            cols = self.col_filtered
        return super().get_rows_by(data, with_rowid, cols)

    def get_rows(self, with_rowid: bool = False, cols: typing.List[str] = None):
        if cols is None:
            cols = self.col_filtered
        return self.get_rows_by({self.title: self.snowflake}, with_rowid, cols=cols)


class UniqueManager(GuildManager):
    """
    Like GuildManager but ensures the key used is unique (ie for messages)
    """
    def __init__(self, sql: SQLManager, table_name: str, columns: typing.List[str], snowflake: int,
                 title: str = 'snowflake'):
        super().__init__(sql, table_name, columns, snowflake, title)

    def _get_identifier_column(self):
        return super()._get_identifier_column() + ' UNIQUE'
